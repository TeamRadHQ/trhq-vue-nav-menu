export default {
    data() {
        return {
            windowWidth  : window.innerWidth,
            windowHeight : window.innerHeight,
        };
    },
    computed: {

    },
    methods: {
        updateViewport() {
            let menuItemWidth = $(this.$el).find('.menu > ul').width();
            if( menuItemWidth ) {
                this.menuItemWidth = Math.ceil(menuItemWidth);
            }
            this.logoWidth      = Math.ceil( $(this.$el).find('.logo').width() );
            this.windowWidth    = window.innerWidth;
            this.windowHeight   = window.innerWidth;
            this.showMobileMenu = false;
        },
    },
    mounted() {
        window.addEventListener('resize', this.updateViewport);
        setTimeout( this.updateViewport, 50);
    },
};
