// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import NavMenu from './components/NavMenu.vue';
import $ from 'jquery';
window.$ = $;
Vue.config.productionTip = false;

let items = [
  {
    label: 'Nav Item 1',
    title: 'Go to Nav item 1',
    href: '#?1',
  },
  {
    label: 'Nav Item 2',
    href: '#?2',
  },
  {
    label: 'Nav Item 3',
    title: 'A title for nav item 3',
    href: '#?3',
  },
  {
    label: 'Nav Item 4',
    href: '#?4',
  },
  {
    label: 'Nav Item 5',
    href: '#?5',
  },
  {
    label: 'Nav Item 6',
    href: '#?6',
  },
  {
    label: 'Nav Item 7',
    href: '#?7',
  },
];
console.log(JSON.stringify(items));
$('nav-menu').attr('items', JSON.stringify(items));
console.log($('nav-menu').length);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: {
        NavMenu,
    },
});
